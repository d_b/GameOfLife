package gol;

import java.text.DecimalFormat;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class GolApplication extends Application {

	private final int WIDTH = 1800;
	private final int HEIGHT = 1000;
	private final Color BACKGROUND_COLOR = Cell.deadColor;
	
	private GraphicsContext graphicsContext;
	private Environment environment;
	
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Game Of Life");
		primaryStage.setResizable(false);
		// necessary to eliminate additional margins that result
		// from disabling ability to resize
		primaryStage.sizeToScene();
		
		Group root = new Group();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		Canvas canvas = new Canvas(WIDTH, HEIGHT);
		root.getChildren().add(canvas);
		
		primaryStage.show();
		
		environment = new Environment(WIDTH, HEIGHT);
		graphicsContext = canvas.getGraphicsContext2D();
		System.out.println("Game Of Life started with " + environment.getAmountOfLivingCells() + " living Cells.");
		// starts the timer that handles drawing on the canvas
		new MainLoop().start();
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	private class MainLoop extends AnimationTimer {
		private long lastCallTimeNanoSec = -1;
		private long lastSecondNanoSec = -1;
		private long lastGenerationNanoSec = -1;
		private long deltaTimeMilliSec = 0;
		
		private long startDrawingNanoSec = -1;
		private long endDrawingNanoSec = -1;
		private double drawingTimeNanoSec = -1;
		
		private long startNextGenerationNanoSec = -1;
		private long endNextGenerationNanoSec = -1;
		private double advancingToNextGenerationTimeNanoSec = -1;
		
		private DecimalFormat formatter = new DecimalFormat("#.###");
		
		// handle method will be called automatically with approximately 60Hz
		public void handle(long currentTimeNanoSec) {
			
			startDrawingNanoSec = System.nanoTime();
				clearCanvas(graphicsContext);
				environment.draw(graphicsContext);
			endDrawingNanoSec = System.nanoTime();
			drawingTimeNanoSec = endDrawingNanoSec - startDrawingNanoSec;
			
			// ignore timers on first call
			if (lastCallTimeNanoSec == -1) {
				lastCallTimeNanoSec = currentTimeNanoSec;
				lastSecondNanoSec = currentTimeNanoSec;
				lastGenerationNanoSec = currentTimeNanoSec;
				return;
			}
			
			deltaTimeMilliSec = nanoSecToMilliSec(currentTimeNanoSec - lastCallTimeNanoSec);
			// print current generation counter, amount of living cells, FPS, drawingTime, generationAdvanceTime
			// each second
			if (deltaTimeMilliSec > 0
				&& nanoSecToMilliSec(currentTimeNanoSec - lastSecondNanoSec) >= 1000)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("Generation: ");
				sb.append(environment.getCurrentGeneration());
				sb.append(" Living cells: ");
				sb.append(environment.getAmountOfLivingCells());
				sb.append(" | ");
				sb.append(1000 / deltaTimeMilliSec);
				sb.append(" FPS | DrawingTime: ");
				sb.append(formatter.format(nanoSecToMilliSec(drawingTimeNanoSec)));
				sb.append("ms | Time to advance to next generation: ");
				sb.append(formatter.format(nanoSecToMilliSec(advancingToNextGenerationTimeNanoSec)));
				sb.append("ms");
				System.out.println(sb.toString());
				
				lastSecondNanoSec = currentTimeNanoSec;
			}
			
			// only advance to next generation if the necessary time has passed
			if (nanoSecToMilliSec(currentTimeNanoSec - lastGenerationNanoSec) >= (1000.0 / environment.getGenerationFrequency())) {
				startNextGenerationNanoSec = System.nanoTime();
					environment.advanceToNextGeneration();
				endNextGenerationNanoSec = System.nanoTime();
				advancingToNextGenerationTimeNanoSec = endNextGenerationNanoSec - startNextGenerationNanoSec;
				lastGenerationNanoSec = currentTimeNanoSec;
			}
			
			lastCallTimeNanoSec = currentTimeNanoSec;
		}
		
		public long nanoSecToMilliSec(long nanoSec){
			return nanoSec / 1000000;
		}
		
		public double nanoSecToMilliSec(double nanoSec){
			return (double)nanoSec / 1000000;
		}
	}
	
	private void clearCanvas(GraphicsContext gc) {
		gc.setFill(BACKGROUND_COLOR);
		gc.fillRect(0, 0, WIDTH, HEIGHT);
	}
}
