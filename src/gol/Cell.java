package gol;

import java.util.ArrayList;

import javafx.scene.paint.Color;

public class Cell {
	
	public enum CellState {
		Dead, Alive
	}
	
	public static final Color aliveColor = Color.LIGHTGREEN;
	public static final Color deadColor = Color.GRAY;
	
	private final int DEATH_BY_LONELINESS = 2;
	private final int DEATH_BY_OVERCROWDING= 3;
	private final int REBIRTH = 3;

	private final int DEATH_BY_OLD_AGE = 50;
	
	private int amountOfLivingNeighbours;

	private int age;
	private CellState state;
	private CellState nextState;
	private ArrayList<Cell> neighbours;
	
	public Cell(CellState state) {
		this.state = state;
		nextState = this.state;
		neighbours = new ArrayList<Cell>(8);
		age = 0;
	}
	
	public boolean isAlive() {
		return state == CellState.Alive;
	}
	
	public boolean isDead() {
		return state == CellState.Dead;
	}
	
	public void addNeighbour(Cell newNeighbour) {
		neighbours.add(newNeighbour);
	}
	
	public void addNeighbours(ArrayList<Cell> newNeighbours) {
		neighbours.addAll(newNeighbours);
	}
	
	// decide whether cell is still alive in next generation
	public void checkIfWouldSurvive() {
		amountOfLivingNeighbours = countLivingNeighbours();
		switch (state) {
		case Alive: 
			if (willBeDeadInNextGeneration()) {
				nextState = CellState.Dead;
			}
			break;
		case Dead:
			if (willBeResurrectedInNextGeneration()) {
				nextState = CellState.Alive;
			}
			break;
		default:
			break;
		}
	}
	
	public void advanceToNextGeneration() {
		switch (nextState) {
		case Alive: 
			if (state == CellState.Dead) {
				state = CellState.Alive;
				age = 0;
			}
			else {
				age++;
			}
			break;
		case Dead:
			state = CellState.Dead;
			break;
		default:
			break;
		}
	}
	
	private int countLivingNeighbours() {
		int livingNeighbours = 0;
		for (Cell c : neighbours) {
			if (c.isAlive()) {
				livingNeighbours++;
			}
		}
		return livingNeighbours;
	}
	
	public int getAge() {
		return age;
	}
	
	public Color getAliveColorRGB() {
		return Color.rgb(100 - age * 2, 255 - age * 5, 60 - age);
	}
	
	private boolean willBeDeadInNextGeneration() {
		boolean willDie = false;
		// if (state == CellState.Alive) {
		willDie = (amountOfLivingNeighbours < DEATH_BY_LONELINESS);
		willDie = willDie || (amountOfLivingNeighbours > DEATH_BY_OVERCROWDING);
		willDie = willDie || (age >= DEATH_BY_OLD_AGE);
		// }
		return willDie;
	}
	
	private boolean willBeResurrectedInNextGeneration() {
		boolean willBeResurrected = false;
		// if (state == CellState.Dead) {
		willBeResurrected = amountOfLivingNeighbours == REBIRTH;
		// }
		return willBeResurrected;
	}
}
