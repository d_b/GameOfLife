package gol;

import gol.Cell.CellState;

import java.util.ArrayList;
import java.util.Random;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Environment {
	
	// environment will be filled with this many percent living cells
	private final double INITIAL_CELLS_ALIVE_RATE = 0.3;
	private final int CELL_AMOUNT_HORIZONTAL = 400;
	private final int CELL_AMOUNT_VERTICAL = 200;
	private final int TOTAL_AMOUNT_CELLS = CELL_AMOUNT_HORIZONTAL * CELL_AMOUNT_VERTICAL;
	private final int GENERATION_FREQUENCY_HZ = 10;
	
	private ArrayList<Cell> cells;
	private int width;
	private int height;
	private int amountOfLivingCells;
	private int generationCounter = 0;
	private double cellWidth;
	private double cellHeight;
	
	public Environment(int width, int height) {
		this.width = width;
		this.height = height;
		cellWidth = (double)this.width / CELL_AMOUNT_HORIZONTAL;
		cellHeight = (double)this.height / CELL_AMOUNT_VERTICAL;
		fillEnvironmentWithCells();
	}
	
	public void draw(GraphicsContext gc) {
		int rowIndex = 0;
		int columnIndex = 0;
		double currentX;
		double currentY;
		
		for (Cell c : cells) {
			currentX = columnIndex * cellWidth;
			currentY = rowIndex * cellHeight;
			
			if (c.isAlive()) {
				gc.setFill(c.getAliveColorRGB());
				gc.fillRect(currentX, currentY, cellWidth, cellHeight);
			}
			
			columnIndex++;
			if (columnIndex > CELL_AMOUNT_HORIZONTAL - 1) {
				columnIndex = 0;
				rowIndex++;
			}	
		}
		
		drawGrid(gc);
	}
	
	private void drawGrid(GraphicsContext gc) {
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(0.2);
		// vertical lines
		double horizontalIncrement = (double)width / CELL_AMOUNT_HORIZONTAL;
		for (double d = 0; d < width; d += horizontalIncrement) {
			gc.strokeLine(d, 0, d, height);
		}
		// horizontal lines
		double verticalIncrement = (double)height / CELL_AMOUNT_VERTICAL;
		for (double d = 0; d < height; d += verticalIncrement) {
			gc.strokeLine(0, d, width, d);
		}
	}
	
	public void advanceToNextGeneration() {
		for (Cell c : cells) {
			c.checkIfWouldSurvive();
		}
		amountOfLivingCells = 0;
		for (Cell c : cells) {
			c.advanceToNextGeneration();
			if (c.isAlive()) {
				amountOfLivingCells++;
			}
		}
		generationCounter++;
	}
	
	public int getAmountOfLivingCells() {
		return amountOfLivingCells;
	}
	
	public int getGenerationFrequency() {
		return GENERATION_FREQUENCY_HZ;
	}
	
	public int getCurrentGeneration() {
		return generationCounter;
	}
	
	private void fillEnvironmentWithCells() {
		cells = new ArrayList<Cell>(TOTAL_AMOUNT_CELLS);
		amountOfLivingCells = 0;
		// decides whether a Cell will start alive or dead
		Random randomGod = new Random();
		
		// create all cells
		for (int i = 0; i < TOTAL_AMOUNT_CELLS; i++) {
			CellState state;
			if (randomGod.nextDouble() > 1.0 - INITIAL_CELLS_ALIVE_RATE) {
				state = CellState.Alive;
				amountOfLivingCells++;
			}
			else {
				state = CellState.Dead;
			}
			Cell c = new Cell(state);
			cells.add(i, c);
		}
		
		// tell each cell who its neighbours are
		for (int i = 0; i < TOTAL_AMOUNT_CELLS; i++) {
			Cell current = cells.get(i);
			// top left corner cell
			if (i == 0) {
				current.addNeighbour(cells.get(1)); // right neighbour
				current.addNeighbour(cells.get(CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
				current.addNeighbour(cells.get(CELL_AMOUNT_HORIZONTAL + 1)); // bottom right neighbour
			}
			// only cells in first row except corners
			else if (i < CELL_AMOUNT_HORIZONTAL - 1) {
				current.addNeighbour(cells.get(i - 1)); // left neighbour
				current.addNeighbour(cells.get(i + 1)); // right neighbour
				current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL - 1)); // bottom left neighbour
				current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
				current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL + 1)); // bottom right neighbour
			}
			// top right corner cell
			else if (i == CELL_AMOUNT_HORIZONTAL - 1) {
				current.addNeighbour(cells.get(i - 1)); // left neighbour
				current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL - 1)); // bottom left neighbour
				current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
			}
			// all cells except those in first and last row
			else if (i >= CELL_AMOUNT_HORIZONTAL
				&& i < (TOTAL_AMOUNT_CELLS - CELL_AMOUNT_HORIZONTAL))
			{
				int iModCellsHorizontal = i % CELL_AMOUNT_HORIZONTAL;
				// only cells in left columm
				if (iModCellsHorizontal == 0) {
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL + 1)); // top right neighbour
					current.addNeighbour(cells.get(i + 1)); // right neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL + 1)); // bottom right neighbour
				}
				// only cells in right column
				else if (iModCellsHorizontal == (CELL_AMOUNT_HORIZONTAL - 1)) {
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL - 1)); // top left neighbour
					current.addNeighbour(cells.get(i - 1)); // left neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL - 1)); // bottom left neighbour
				}
				// the majority of cells. those that have a full set of 8 neighbours
				else {
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL - 1)); // top left neighbour
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
					current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL + 1)); // top right neighbour
					current.addNeighbour(cells.get(i - 1)); // left neighbour
					current.addNeighbour(cells.get(i + 1)); // right neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL - 1)); // bottom left neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL)); // bottom neighbour
					current.addNeighbour(cells.get(i + CELL_AMOUNT_HORIZONTAL + 1)); // bottom right neighbour
				}
			}
			// bottom left corner cell
			else if (i == TOTAL_AMOUNT_CELLS - CELL_AMOUNT_HORIZONTAL) {
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL + 1)); // top right neighbour
				current.addNeighbour(cells.get(i + 1)); // right neighbour
			}
			// bottom right corner cell
			else if (i == TOTAL_AMOUNT_CELLS - 1) {
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL - 1)); // top left neighbour
				current.addNeighbour(cells.get(i - 1)); // left neighbour
			}
			// only cells in last row except corners
			else if (i > (TOTAL_AMOUNT_CELLS - CELL_AMOUNT_HORIZONTAL)) {
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL - 1)); // top left neighbour
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL)); // top neighbour
				current.addNeighbour(cells.get(i - CELL_AMOUNT_HORIZONTAL + 1)); // top right neighbour
				current.addNeighbour(cells.get(i - 1)); // left neighbour
				current.addNeighbour(cells.get(i + 1)); // right neighbour
			}
		}
	}
	
}
